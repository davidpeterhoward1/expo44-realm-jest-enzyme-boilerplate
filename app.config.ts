import { ExpoConfig } from "@expo/config-types";

const config: ExpoConfig = {
  name: "Dosen",
  slug: "dosen",
};

export default config;
