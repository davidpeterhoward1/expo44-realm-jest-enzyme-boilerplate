import React, { ReactElement, useContext, useState, ReactNode } from "react";

interface AuthContext {
  transactionMenuOpen: string;
  setTransactionMenuOpen: (argo0: string) => void;
}

const AuthContext = React.createContext({} as AuthContext);

/**
 * AuthContext & AuthProvider
 ** For control over Auths or Auth-like things
 ** as to not pollute the AuthProvider as much
 */
const AuthProvider = ({ children }: { children: ReactNode }): ReactElement => {
  const [transactionMenuOpen, setTransactionMenuOpen] = useState("");

  return (
    <AuthContext.Provider
      value={{
        transactionMenuOpen,
        setTransactionMenuOpen,
      }}>
      {children}
    </AuthContext.Provider>
  );
};

// * The useAuth hook can be used by components under an AuthProvider to
// * access the auth context value.
const useAuth = (): AuthContext => {
  const auth = useContext(AuthContext);
  if (auth == null) {
    throw new Error("useAuth() called outside of a AuthProvider?");
  }
  return auth;
};

export { AuthProvider, useAuth };
