import Realm from "../NativeFallbacks/Realm";

const app = new Realm.App({ id: "dosen-pluxx" });

export default app;
