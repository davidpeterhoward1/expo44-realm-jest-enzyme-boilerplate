import React, { ReactElement, useContext, useState, ReactNode } from "react";
import app from "./RealmApp";

interface RealmContext {
  app: Realm.App;
}

const RealmContext = React.createContext({} as RealmContext);

/**
 *? RealmContext & RealmProvider
 ** For control over Realms or Realm-like things
 ** as to not pollute the AuthProvider as much
 */
const RealmProvider = ({ children }: { children: ReactNode }): ReactElement => {
  return (
    <RealmContext.Provider
      value={{
        app,
      }}>
      {children}
    </RealmContext.Provider>
  );
};

// * The useRealm hook can be used by components under an RealmProvider to
// * access the auth context value.
const useRealm = (): RealmContext => {
  const realm = useContext(RealmContext);
  if (Realm == null) {
    throw new Error("useRealm() called outside of a RealmProvider?");
  }
  return realm;
};

export { RealmProvider, useRealm };
