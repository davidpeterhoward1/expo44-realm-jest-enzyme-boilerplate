import { Wallet } from "ethers";

import mockWallet from "./__mocks__/mockWallet";
import createNewWallet from "./CreateWallet";

jest.mock("ethers", () => ({
  Wallet: { createRandom: jest.fn().mockImplementation(() => mockWallet as Wallet) },
}));

it("Mocks createNewWallet", () => {
  const wrapper = createNewWallet();
  expect(wrapper).toStrictEqual(mockWallet);
});
