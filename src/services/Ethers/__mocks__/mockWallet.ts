const mockWallet = {
  _isSigner: true,
  mnemonic: {
    phrase: "jaguar oven hedgehog decorate cigar awesome mercy answer envelope wheat hope viable",
  },
  privateKey: "0x71e5e8071e9808a50f24e20fd2cb0ec5322f18ddc12af141fe739f78eae7f474",
  _mnemonic: () => {},
  _signingKey: () => {},
  address: "0xEcDd6fAe72f325293b6425811Fd3E2Cf44E423fd",
  provider: null,
};

export default mockWallet;
