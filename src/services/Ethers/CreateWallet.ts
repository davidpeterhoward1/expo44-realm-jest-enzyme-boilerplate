import { Wallet } from "ethers";

const createNewWallet = (): Wallet => {
  return Wallet.createRandom();
};

export default createNewWallet;


