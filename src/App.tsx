import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Platform } from "react-native";
import * as DevMenu from "expo-dev-menu";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { Wallet } from "ethers";
import * as KeyChain from "react-native-keychain";
import NativeBaseThemeProvider from "./providers/Theme/NatieBaseThemeProvider";
import { RealmProvider } from "./providers/Realm/RealmProvider";
import { AuthProvider } from "./providers/Auth/AuthProvider";

const Button = ({ label, onPress }) => (
  <TouchableOpacity style={styles.buttonContainer} onPress={onPress}>
    <Text style={styles.buttonText}>{label}</Text>
  </TouchableOpacity>
);

const HomeScreen = ({ navigation }: any) => {
  const [newWallet, setNewWallet] = React.useState<Wallet | null>(null);
  const [encryptedWallet, setEncryptedWallet] = React.useState<string | null>(null);
  const [phrase, setPhrase] = useState<string | null>(null);

  const handleCreateWallet = () => {
    const newWallet = Wallet.createRandom();
    const wally = newWallet?.mnemonic?.phrase;
    setPhrase(wally);
    setNewWallet(newWallet);
    console.log(newWallet);
  };

  const decryptWallet = async () => {
    try {
      const decrypt = await Wallet.fromEncryptedJson(encryptedWallet!, "Pa$$wo4d");
      console.log(decrypt);
    } catch (error) {
      console.log(error);
    }
  };
  const handleEncrypt = async () => {
    try {
      const encrypt = await newWallet.encrypt("Pa$$wo4d");
      setEncryptedWallet(encrypt);
      console.log(encrypt);
    } catch (error) {
      console.log(error);
    }
  };

  const addWalletToKeychain = () => {
    try {
      const addWallet = KeyChain.setGenericPassword(`${newWallet}`, `${"my wallet"}`, { service: "wallet" });
      console.log(addWallet);
    } catch (error) {
      console.log(error);
    }
  };

  const walletFromPhrase = () => {
    try {
      const result = Wallet.fromMnemonic(phrase);
      console.log(result);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <View>
      <Button label={"hey about!"} onPress={() => navigation.navigate("About")} />
      <Button label={"wallet!"} onPress={() => handleCreateWallet()} />
      <Button label={"encrypt!"} onPress={() => handleEncrypt()} />
      <Button label={"decrypt!!"} onPress={() => decryptWallet()} />
      <Button label={"walletFromPhrase!!"} onPress={() => walletFromPhrase()} />
      <Text>Hey home!</Text>
      <Text>Hey home!</Text>
      <Text>Hey home!</Text>
      <Text>Hey home!</Text>
      <Text>Hey home!</Text>
      <Text>Hey home!</Text>
      <Text>Hey home!</Text>
      <Text>Hey home!</Text>
      <Text>Hey home!</Text>
      <Text>Hey home!</Text>
      <Text>Hey home!</Text>
    </View>
  );
};
const AboutScreen = () => {
  return (
    <View>
      <Button
        label="Open Dev Menu"
        onPress={() => {
          DevMenu.openMenu();
        }}
      />
      <Text>Hey About!</Text>
      <Text>Hey About!</Text>
      <Text>Hey About!</Text>
      <Text>Hey About!</Text>
      <Text>Hey About!</Text>
      <Text>Hey About!</Text>
      <Text>Hey About!</Text>
      <Text>Hey About!</Text>
      <Text>Hey About!</Text>
      <Text>Hey About!</Text>
      <Text>Hey About!</Text>
    </View>
  );
};

const Main = createNativeStackNavigator();

const MainNavigator = () => {
  return (
    <NavigationContainer>
      <Main.Navigator>
        <Main.Screen name="Home" component={HomeScreen} />
        <Main.Screen name="About" component={AboutScreen} />
      </Main.Navigator>
    </NavigationContainer>
  );
};

export default function App() {
  console.log(Platform.OS);
  return (
    <NativeBaseThemeProvider>
      <RealmProvider>
        <AuthProvider>
          <MainNavigator />
        </AuthProvider>
      </RealmProvider>
    </NativeBaseThemeProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonContainer: {
    backgroundColor: "#4630eb",
    borderRadius: 4,
    padding: 12,
    marginVertical: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonText: {
    color: "#ffffff",
    fontSize: 16,
  },
});

export const ThisFunc = () => {
  return (
    <View>
      <Text>Hey</Text>
    </View>
  );
};
