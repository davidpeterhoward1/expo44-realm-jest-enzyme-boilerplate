import { shallow } from "enzyme";
import React from "react";
import { View, StyleSheet, Platform } from "react-native";
import ThisFunc from "./AppTester";
// Note: test renderer must be required after react-native.
import renderer from "react-test-renderer";

it("renders <App /> without crashing", () => {
  const wrapper = shallow(<ThisFunc />);
  expect(wrapper).toHaveLength(1); //?
});

it("renders correctly", () => {
  renderer.create(<ThisFunc />);
});
