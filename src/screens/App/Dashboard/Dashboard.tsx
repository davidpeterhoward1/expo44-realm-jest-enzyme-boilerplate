// The config also be placed into package.json or global quokka config,
// see https://quokkajs.com/docs/configuration.html
({
  babel: true,
  plugins: ["jsdom-quokka-plugin"],
});
import React, { ReactElement } from "react";
import { View, Text, NativeBaseProvider } from "native-base";
import ReactDOM from "react-dom";

document.body.innerHTML += `<div id="root"></div>`;

ReactDOM.render(
  <NativeBaseProvider>
    <View>
      <Text>Hello, world</Text>
    </View>
  </NativeBaseProvider>,
  document.getElementById("root")
);

// const root = document.getElementById("root").innerHTML;
// root; //?

// export default function Dashboard({}): ReactElement {
//   return (
//     <View>
//       <Text>Hello, world</Text>
//     </View>
//   );
// }
